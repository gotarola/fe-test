[![pipeline status](https://gitlab.com/gotarola/fe-test/badges/master/pipeline.svg)](https://gitlab.com/gotarola/fe-test/commits/master)

# FE Test

#### Prerequisites Technologies

1. Install:
- [Node.js](https://nodejs.org/en/download/)
- [MongoDB](https://www.mongodb.com/download-center)

2. Config:
- Set your mongodb config in `<root>/src/config/config.<NODE_ENV>.json` file


## ENV Running
---

- **Develop:**

```sh
npm i
npm run dev
```

Running on http://localhost:3000

## Project structure
------------
    |-- src
    |   |-- modules
    |       |-- address
    |       |-- index.js
    |       |-- **collections**
    |   |-- config
    |       |-- *.json
    |       |-- index.js
    |   |-- db
    |       |-- mongo.js
    |   |-- middlewares
    |       |-- ssr.js
    |   |-- views
    |       |-- actions
    |       |-- components
    |       |-- containers
    |       |-- reducers
    |       |-- App.js
    |       |-- BaseComponent.js
    |       |-- browser.js
    |       |-- store.js
    |   |-- index.js
    |   |-- errors.js