import React from 'react';
import ReactDOMServer from 'react-dom/server';
import BaseComponent from '../views/BaseComponent';

export default () => (req, res, next) => {
  res.locals.ssr = function SSR(props) {
    const content = ReactDOMServer.renderToStaticMarkup(<BaseComponent />);

    return `<!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta content="width=device-width,initial-scale=1" name=viewport>
        <link rel="stylesheet" href="${props.cssUrl}">
        <title>${props.pageTitle}</title>
      </head>
      <body>
        <div id="root">${content}</div>
        <script>window.__APP_INITIAL_STATE__ = ${JSON.stringify({})}</script>
        <script src="${props.jsUrl}"> </script> 
      </body>
      </html>
    `;
  };

  next();
};
