import webpack from 'webpack';
import bodyParser from 'body-parser';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpackHotServerMiddleware from 'webpack-hot-server-middleware';

import webpackConfig from '../../webpack.config';
import webpackConfigServer from '../../webpack.config.server';

module.exports = (app) => {
  app.disable('x-powered-by');

  app.use(bodyParser.json());

  if (process.env.NODE_ENV === 'development') {
    const compiler = webpack([webpackConfig, webpackConfigServer]);

    const [clientCompiler] = compiler.compilers;

    const _webpackDevMiddleware = webpackDevMiddleware(
      compiler,
      webpackConfig.devServer,
    );

    const _webpackHotMiddlware = webpackHotMiddleware(
      clientCompiler,
      webpackConfig.devServer,
    );

    app.use(_webpackDevMiddleware);
    app.use(_webpackHotMiddlware);
    app.use(webpackHotServerMiddleware(compiler));
  }
};
