import { compose, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import rootReducer from './reducers';

let store;
const composeEnhancers = typeof window === 'object'
  && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose;

const configureStore = (initialState) => {
  if (!store) {
    const enhancer = composeEnhancers(
      applyMiddleware(thunk),
    );

    store = createStore(
      rootReducer,
      initialState,
      enhancer
    );
  }

  return store;
};

export default configureStore;
