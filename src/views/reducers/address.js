/* eslint-disable complexity */
import {
  ADDRESS_ID_INPUT_HANDLER,
  ADDRESS_ID_INPUT_REQUEST,
  ADDRESS_ID_INPUT_REQUEST_SUCCESS,
  ADDRESS_ID_INPUT_REQUEST_FAILURE,
  TRANSACTION_PAGINATION_REQUEST,
  TRANSACTION_PAGINATION_REQUEST_SUCCESS,
  TRANSACTION_PAGINATION_REQUEST_FAILURE
} from '../constants';

const DEFAULT_STAGE = {
  error: null,
  balance: null,
  addressId: '',
  isLoading: false,
  transactions: {
    page: 1,
    elements: [],
  },
};

const addressReducer = (state = DEFAULT_STAGE, action) => {
  switch (action.type) {
    case ADDRESS_ID_INPUT_HANDLER: {
      return {
        ...state,
        addressId: action.addressId,
      };
    }
    case ADDRESS_ID_INPUT_REQUEST: {
      return {
        ...state,
        error: null,
        balance: null,
        isLoading: true,
        transactions: {
          page: 1,
          elements: [],
        },
      };
    }
    case ADDRESS_ID_INPUT_REQUEST_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        balance: action.data.balance,
        transactions: {
          ...state.transactions,
          elements: action.data.transactions,
        },
      };
    }
    case ADDRESS_ID_INPUT_REQUEST_FAILURE: {
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    }
    case TRANSACTION_PAGINATION_REQUEST: {
      return {
        ...state,
        transactions: {
          ...state.transactions,
          error: null,
          isLoading: true,
        },
      };
    }
    case TRANSACTION_PAGINATION_REQUEST_SUCCESS: {
      return {
        ...state,
        transactions: {
          ...state.transactions,
          isLoading: false,
          page: action.data.page,
          elements: action.data.transactions,
        },
      };
    }
    case TRANSACTION_PAGINATION_REQUEST_FAILURE: {
      return {
        ...state,
        transactions: {
          ...state.transactions,
          isLoading: false,
          error: action.error,
        },
      };
    }
    default: {
      return state;
    }
  }
};

export default addressReducer;
