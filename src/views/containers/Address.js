import React from 'react';
import { connect } from 'react-redux';

import Addresss from '../components/Addresss';
import { getAddress, getTransactions, addressIdInputHandler } from '../actions/address';

const AddressContainer = (props) => (
  <Addresss {...props} />
);

const mapStateToProps = (state) => ({
  address: state.address,
});

const mapDispatchToProps = (dispatch) => ({
  getAddress: (addressId) => (
    dispatch(getAddress(addressId))
  ),
  getTransactions: (page, addressId) => (
    dispatch(getTransactions(page, addressId))
  ),
  addressIdInputHandler: (addressId) => (
    dispatch(addressIdInputHandler(addressId))
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddressContainer);
