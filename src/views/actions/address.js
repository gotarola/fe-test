import axios from 'axios';

import {
  ADDRESS_ID_INPUT_HANDLER,
  ADDRESS_ID_INPUT_REQUEST,
  ADDRESS_ID_INPUT_REQUEST_SUCCESS,
  ADDRESS_ID_INPUT_REQUEST_FAILURE,
  TRANSACTION_PAGINATION_REQUEST,
  TRANSACTION_PAGINATION_REQUEST_SUCCESS,
  TRANSACTION_PAGINATION_REQUEST_FAILURE
} from '../constants';

const addressIdInputHandler = (addressId) => ({
  addressId,
  type: ADDRESS_ID_INPUT_HANDLER,
});

const getAddress = (addressId) => (
  async (dispatch) => {
    dispatch({
      type: ADDRESS_ID_INPUT_REQUEST,
    });

    try {
      const { data } = await axios.get(`/address/${addressId}`);

      return dispatch({
        data,
        type: ADDRESS_ID_INPUT_REQUEST_SUCCESS,
      });
    } catch (error) {
      return dispatch({
        error: error.response.data,
        type: ADDRESS_ID_INPUT_REQUEST_FAILURE,
      });
    }
  }
);

const getTransactions = (page) => (
  async (dispatch, getState) => {
    dispatch({
      type: TRANSACTION_PAGINATION_REQUEST,
    });

    try {
      const { data } = await axios.get(`/address/${getState().address.addressId}/transactions?page=${page}`);
      data.page = page;

      return dispatch({
        data,
        type: TRANSACTION_PAGINATION_REQUEST_SUCCESS,
      });
    } catch (error) {
      return dispatch({
        error: error.response.data,
        type: TRANSACTION_PAGINATION_REQUEST_FAILURE,
      });
    }
  }
);

export {
  getAddress,
  getTransactions,
  addressIdInputHandler
};
