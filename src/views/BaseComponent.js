import React from 'react';
import { Provider } from 'react-redux';

import configureStore from './store';
import App from './App';

import './styles.css';

class BaseComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const store = configureStore();

    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}

export default BaseComponent;
