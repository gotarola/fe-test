import '@babel/polyfill';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import BaseComponent from './BaseComponent';

function renderComponent(Component) {
  ReactDOM.hydrate(
    <AppContainer>
      <Component />
    </AppContainer>,
    document.getElementById('root')
  );
}

renderComponent(BaseComponent);

if (module.hot) {
  module.hot.accept('./App.js', () => {
    // eslint-disable-next-line global-require
    const NewApp = require('./BaseComponent').default;

    renderComponent(NewApp);
  });
}
