import React from 'react';

import Main from './components/Main';
import Navbar from './components/Navbar';

class Content extends React.Component {
  render() {
    return (
      <div>
        <Navbar />
        <Main />
      </div>
    );
  }
}

export default Content;
