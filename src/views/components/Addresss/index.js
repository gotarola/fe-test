import React from 'react';
import PropTypes from 'prop-types';

import Transactions from './Transactions';

import './styles.css';

class Address extends React.Component {
  inputHandler = (e) => {
    this.props.addressIdInputHandler(e.target.value);
  };

  getAddress = (addressId) => (
    (e) => {
      e.preventDefault();

      this.props.getAddress(addressId);
    }
  );

  getAddressInfo = (error, balance, transactions) => {
    if (error) {
      return (
        <p className="text-danger">
          <strong>{error.result}</strong>
        </p>
      );
    }

    if (balance && transactions.elements.length > 0) {
      return (
        <div className="address-info">
          <p><strong>Balance:&nbsp;</strong>{balance}</p>
          <Transactions {...transactions} getTransactions={this.props.getTransactions} />
        </div>
      );
    }

    return null;
  };

  getButtonContent = (isLoading) => {
    if (isLoading) {
      return (
        <i className="fa fa-circle-o-notch fa-spin" />
      );
    }

    return 'Search';
  }

  render() {
    const {
      error,
      balance,
      isLoading,
      addressId,
      transactions,
    } = this.props.address;

    const buttonContent = this.getButtonContent(isLoading);
    const addressInfo = this.getAddressInfo(error, balance, transactions);

    return (
      <div>
        <div className="address-search-container">
          <input
            type="search"
            aria-label="Search"
            placeholder="Search address"
            className="form-control mr-sm-2"
            value={addressId}
            onChange={this.inputHandler}
          />
          <button
            type="submit"
            className="btn btn-secondary search-button"
            disabled={isLoading}
            onClick={this.getAddress(addressId)}
          >
            {buttonContent}
          </button>
        </div>
        {addressInfo}
      </div>
    );
  }
}

Address.propTypes = {
  address: PropTypes.object.isRequired,
  getAddress: PropTypes.func.isRequired,
  getTransactions: PropTypes.func.isRequired,
  addressIdInputHandler: PropTypes.func.isRequired,
};

export default Address;
