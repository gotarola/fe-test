import React from 'react';
import PropTypes from 'prop-types';

class Transactions extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      filter: '',
    };
  }

  inputHandler = (e) => {
    this.setState({
      filter: e.target.value,
    });
  };

  getPage = (page) => (
    (e) => {
      e.preventDefault();

      this.props.getTransactions(page);
    }
  );

  getRows = (rows) => {
    if (rows.length === 0) {
      return (
        <tr>
          <td className="text-center" colSpan="3">No transactions :(</td>
        </tr>
      );
    }

    return rows
      .filter((row) => {
        if (this.state.filter.length === 0) {
          return true;
        }

        return row.blockNumber.indexOf(this.state.filter) !== -1;
      })
      .map((row, index) => (
        <tr key={`address-row-${index}`}>
          <td>{row.blockNumber}</td>
          <td>{row.from}</td>
          <td>{row.to}</td>
        </tr>
      ));
  };

  getFooter = (page, isLoading) => {
    if (this.state.filter.length > 0) {
      return null;
    }

    return (
      <tfoot>
        <tr>
          <td colSpan="3">
            <div className="transactions-table-footer">
              <button
                disabled={isLoading}
                onClick={this.getPage(page + 1)}
                className="btn btn-secondary btn-sm"
              >
                Next
                  </button>
              <span>{`Page ${page}`}</span>
              <button
                disabled={isLoading || page === 1}
                onClick={this.getPage(page - 1)}
                className="btn btn-secondary btn-sm"
              >
                Prev.
                  </button>
            </div>
          </td>
        </tr>
      </tfoot>
    );
  };

  getErrorMsg = (error) => {
    if (!error) {
      return (
        <br />
      );
    }

    return (
      <p className="text-danger">
        <strong>{error.result}</strong>
      </p>
    );
  }

  render() {
    const {
      page,
      error,
      elements,
      isLoading,
    } = this.props;

    const rows = this.getRows(elements);
    const footer = this.getFooter(page, isLoading);
    const errorMsg = this.getErrorMsg(error);

    return (
      <div>
        <p><strong>Transactions:</strong></p>
        <input
          type="text"
          aria-label="TransactionFilter"
          placeholder="Filter by block number..."
          className="form-control form-control-sm col-3"
          value={this.state.filter}
          onChange={this.inputHandler}
        />
        {errorMsg}
        <table className="table table-light transactions-table">
          <thead className="thead-dark">
            <tr>
              <th>Block Number</th>
              <th>From</th>
              <th>To</th>
            </tr>
          </thead>
          <tbody>
            {rows}
          </tbody>
          {footer}
        </table>
      </div>
    );
  }
}

Transactions.propTypes = {
  page: PropTypes.number,
  error: PropTypes.object,
  elements: PropTypes.array,
  isLoading: PropTypes.bool,
  getTransactions: PropTypes.func,
};

export default Transactions;
