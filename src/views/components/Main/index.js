import React from 'react';

import AddresssContainer from '../../containers/Address';

import './styles.css';

class Main extends React.Component {
  render() {
    return (
      <div className="container custom-container">
        <AddresssContainer />
      </div>
    );
  }
}

export default Main;
