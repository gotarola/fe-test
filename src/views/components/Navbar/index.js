import React from 'react';

import './styles.css';

class Navbar extends React.Component {
  render() {
    return (
      <nav className="navbar navbar-dark bg-dark custom-navbar">
        <div className="container">
          <a className="navbar-brand" href="#">FE Test</a>
        </div>
      </nav>
    );
  }
}

export default Navbar;
