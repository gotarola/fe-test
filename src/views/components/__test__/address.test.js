import React from 'react';
import renderer from 'react-test-renderer';

import { getAddress, getTransactions, addressIdInputHandler } from '../../actions/address';

import Address from '../Addresss';
import dummyData from './dummy-data.json';

describe('<Address />', () => {
  it('{ initial view }', () => {
    const addressState = {
      error: null,
      balance: null,
      addressId: '',
      isLoading: false,
      transactions: {
        page: 1,
        elements: [],
      },
    };

    const component = (
      <Address
        address={addressState}
        getAddress={getAddress}
        getTransactions={getTransactions}
        addressIdInputHandler={addressIdInputHandler}
      />
    );

    const tree = renderer.create(component).toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('{ dummy data }', () => {
    const component = (
      <Address
        address={dummyData}
        getAddress={getAddress}
        getTransactions={getTransactions}
        addressIdInputHandler={addressIdInputHandler}
      />
    );

    const tree = renderer.create(component).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
