import { getAddressCtrl, transactionsCtrl } from './controllers';

export default (app) => {
  app.get('/address/:addressId', getAddressCtrl);
  app.get('/address/:addressId/transactions', transactionsCtrl);
};
