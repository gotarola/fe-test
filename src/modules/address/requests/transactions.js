import axios from 'axios';

export default async function getTransactionsRequest(payload) {
  const {
    addressId,
    page = 1,
    offset = 10,
  } = payload;

  return new Promise(async (resolve) => {
    const url = `https://api.etherscan.io/api?module=account&action=txlist&address=${addressId}&startblock=0&endblock=99999999&page=${page}&offset=${offset}&sort=asc&apikey=UZUGCFPTZTD6CHY1R665P6XJJGHWZYTC2X`;

    const result = await axios.get(url);

    resolve(result.data);
  });
}
