import axios from 'axios';

export default async function getBalanceRequest(payload) {
  return new Promise(async (resolve) => {
    const url = `https://api.etherscan.io/api?module=account&action=balance&address=${payload.addressId}&tag=latest&apikey=UZUGCFPTZTD6CHY1R665P6XJJGHWZYTC2X`;

    const result = await axios.get(url);

    resolve(result.data);
  });
}
