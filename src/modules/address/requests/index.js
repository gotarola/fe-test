import getBalanceRequest from './balance';
import getTransactionsRequest from './transactions';

export const getAddressRequests = async function getAddressRequest(payload) {
  const balance = await getBalanceRequest(payload);

  if (balance.status === '0') {
    return {
      error: {
        ...balance,
      },
    };
  }

  const transactions = await getTransactionsRequest(payload);

  return {
    balance: balance.result,
    transactions: transactions.result,
  };
};

export const getTransactionsRequests = async function getTransactionsRequests(payload) {
  const transactions = await getTransactionsRequest(payload);

  if (transactions.status === '0') {
    return {
      error: {
        ...transactions,
      },
    };
  }

  return {
    transactions: transactions.result,
  };
};
