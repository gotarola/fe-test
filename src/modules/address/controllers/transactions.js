import { getTransactionsRequests } from '../requests';

export default async function transactionsCtrl(req, res) {
  const payload = {
    page: req.query.page,
    addressId: req.params.addressId,
  };

  const { error, transactions } = await getTransactionsRequests(payload);

  if (error) {
    res.status(400).send(error);
    return;
  }

  res.status(200).send({
    transactions,
  });
}
