import { AddressSchema } from '../schemas';
import { getAddressRequests } from '../requests';

export default async function getAddressCtrl(req, res) {
  const { error, balance, transactions } = await getAddressRequests({ ...req.params });

  if (error) {
    res.status(400).send(error);
    return;
  }

  await AddressSchema.findOneAndUpdate(
    { _id: req.params.addressId },
    { balance: balance },
    { upsert: true, new: true }
  );

  res.status(200).send({
    balance,
    transactions,
  });
}
