import mongoose from 'mongoose';

const { Schema } = mongoose;

const transactionSchema = new Schema({
  to: String,
  gas: String,
  hash: String,
  from: String,
  value: String,
  nonce: String,
  input: String,
  isError: String,
  gasUsed: String,
  gasPrice: String,
  timeStamp: String,
  blockHash: String,
  confirmations: String,
  contractAddress: String,
  txreceipt_status: String,
  transactionIndex: String,
  cumulativeGasUsed: String,
  blockNumber: {
    type: String,
    unique: true,
  },
  address: {
    ref: 'Address',
    type: String,
  },
});

export default mongoose.model('Transaction', transactionSchema);
