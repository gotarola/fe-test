import mongoose from 'mongoose';

const { Schema } = mongoose;

const addressSchema = new Schema({
  _id: String,
  balance: String,
});

export default mongoose.model('Address', addressSchema);
