import addressRoutes from './address/routes';

export default (app) => {
  addressRoutes(app);

  // init path
  app.get('/', (req, res) => {
    const props = {
      jsUrl: 'bundle.js',
      cssUrl: 'main.css',
      pageTitle: 'FE Test',
    };

    const html = res.locals.ssr(props);

    res.send(html);
  });
};
