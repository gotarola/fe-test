import express from 'express';
import _config from 'config.json';

import errors from './errors';
import modules from './modules';
import configServer from './config';
import mongoDbConnection from './db/mongo';

const port = process.env.PORT || 3000;
const SIGNALS = ['SIGHUP', 'SIGINT', 'SIGTERM'];

const app = express();

configServer(app);
modules(app);
errors(app);

const exitApp = (signal) => {
  // eslint-disable-next-line
  console.log(`Shoutdown signal: ${signal}`);

  mongoDbConnection.close(() => {
    process.exit();
  });
};

SIGNALS.forEach((signal) => {
  process.on(signal, () => {
    exitApp(signal);
  });
});

process.on('exit', (code) => {
  // eslint-disable-next-line no-console
  console.log(`Exit with code: ${code}`);
});

const { NODE_ENV } = process.env;
const appConfig = _config('./src/config/config.json', NODE_ENV);

appConfig.ENV = NODE_ENV;

mongoDbConnection.start(appConfig, (error) => {
  if (error) {
    // eslint-disable-next-line
    console.error('Could not connect to MongoDB!');
    process.exit(1);
    return;
  }

  app.listen(port, '0.0.0.0', (err) => {
    if (err) {
      // eslint-disable-next-line
      console.log('Unable to listen for connections ', err);
      process.exit(1);
    }

    /* eslint-disable */
    console.log('--');
    console.log(`Environment:\t\t\t\t${NODE_ENV}`);
    console.log(`Port:\t\t\t\t\t${port}`);
    console.log(`Database:\t\t\t\t${appConfig.db.uri}`);
    /* eslint-enable */
  });
});
