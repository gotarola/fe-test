import mongoose from 'mongoose';

// mongodb connection
const start = (config, cb) => {
  const uri = `mongodb://${config.db.uri}:${config.db.port}/${config.db.name}`;

  mongoose.connect(uri, { useNewUrlParser: true }, (error) => {
    if (error) {
      cb(error);
    }

    cb(null, mongoose);
  });
};

const close = (cb) => {
  mongoose.disconnect(() => {
    // eslint-disable-next-line
    console.log('Mongoose default connection disconnected through app termination');

    cb();
  });
};

module.exports = {
  start,
  close,
};
