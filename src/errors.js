const INTERNAL_ERROR = 500;

const errorLogger = (err, req, res, next) => {
  // eslint-disable-next-line
  console.error(err.stack);
  next(err);
};

const clientErrorHandler = (err, req, res, next) => {
  if (req.xhr) {
    res.status(INTERNAL_ERROR).send({ error: 'We are sorry, we had a problem doing the request' });
  } else {
    next(err);
  }
};

// eslint-disable-next-line no-unused-vars
const errorHandler = (err, req, res, next) => {
  res.status(INTERNAL_ERROR).send({ error: 'We are sorry, intertar error.' });
};

export default (app) => {
  app.use(errorLogger);
  app.use(clientErrorHandler);
  app.use(errorHandler);
};
