const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const nodeExternals = require('webpack-node-externals');
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');

const plugins = [
  new webpack.optimize.LimitChunkCountPlugin({
    maxChunks: 1,
  }),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('development'),
    },
  }),
];

const rules = [
  {
    test: /\.js$/,
    exclude: /node_modules/,
    use: [{
      loader: 'babel-loader',
    }],
  }, {
    test: /\.css$/,
    exclude: /node_modules/,
    use: [
      {
        loader: 'css-loader/locals',
      },
      {
        loader: 'postcss-loader',
        options: {
          plugins: () => [
            autoprefixer(),
          ],
        },
      },
    ],
  },
  {
    loader: 'url-loader',
    test: /\.(eot|woff|woff2|ttf|svg|png|jpe?g|gif)(\?\S*)?$/,
    options: {
      limit: '100000',
      outputPath: 'assets/',
      emitFile: false,
    },
  },
];

const config = {
  plugins,
  name: 'server',
  mode: 'development',
  target: 'node',
  externals: [nodeExternals()],
  module: {
    rules,
  },
  entry: ['./src/middlewares/ssr'],
  output: {
    libraryTarget: 'commonjs2',
    filename: 'dev-server-bundle.js',
    path: path.resolve(process.cwd(), 'build'),
  },
};

module.exports = config;
