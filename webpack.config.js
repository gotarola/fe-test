const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');

const devMode = process.env.NODE_ENV !== 'production';

const plugins = [
  new ExtractCssChunks({
    filename: '[name].css',
    chunkFilename: '[name]-[hash:8].css',
    hot: true,
  }),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('development'),
      WEBPACK: true,
    },
  }),
  new webpack.HotModuleReplacementPlugin(),
];

const rules = [
  {
    test: /\.js$/,
    exclude: /node_modules/,
    use: [{
      loader: 'babel-loader',
    }],
  }, {
    test: /\.css$/,
    use: [
      ExtractCssChunks.loader,
      {
        loader: 'css-loader',
      },
      {
        loader: 'postcss-loader',
        options: {
          plugins: () => [
            autoprefixer(),
          ],
        },
      },
    ],
  },
  {
    loader: 'url-loader',
    test: /\.(eot|woff|woff2|ttf|svg|png|jpe?g|gif)(\?\S*)?$/,
    options: {
      limit: '100000',
      outputPath: 'assets/',
    },
  },
];

module.exports = {
  plugins,
  name: 'client',
  mode: 'development',
  devtool: 'source-map',
  module: {
    rules,
  },
  stats: {
    colors: true,
    reasons: true,
    chunks: true,
  },
  entry: [
    'react-hot-loader/patch',
    '@babel/runtime/regenerator',
    'webpack-hot-middleware/client?reload=true',
    path.resolve(process.cwd(), 'src/views/browser.js'),
  ],
  output: {
    path: path.resolve(process.cwd(), 'dist'),
    publicPath: '/',
    filename: 'bundle.js',
  },
};
