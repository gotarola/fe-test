require('@babel/register');
require('@babel/polyfill');

if (process.env.NODE_ENV === 'development') {
  require.extensions['.css'] = () => (
    null
  );
}

require('./src');
